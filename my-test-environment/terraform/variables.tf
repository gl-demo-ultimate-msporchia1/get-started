variable "prefix" {
  default = "my-test-environment"
}

variable "region" {
  default = "eu-west-1"
}

variable "ssh_public_key_file" {
  default = "/gitlab-environment-toolkit/keys/get-sporchia-test.pub"
}

variable "external_ip_allocation" {
  default = "eipalloc-068f72da2260836c8"
}