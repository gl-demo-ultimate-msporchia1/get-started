terraform {
  backend "s3" {
    bucket = "sporchia-laptop-terraform-state"
    key    = "my-test-environment.tfstate"
    region = "eu-west-1"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.region
}
