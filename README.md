# GitLab Environment Toolkit (GET) - Quickstart

Welcome to the GET Getting Started repository! This guide will help you set up and explore the basics of the GitLab Environment Toolkit.

## Table of Contents
- [What is GET?](#what-is-get)
- [Installation](#installation)
- [Getting Started](#getting-started)
- [Examples](#examples)

## What is GET?
The GitLab Environment Toolkit (GET) is a powerful set of tools designed to streamline and automate the management of environments in GitLab.


## Installation
To get started with this Overview, you'll first need to clone this git repository's structure.
Follow these steps:

1. Start from a GET example or use the 1k example base already provided in this repository: [here](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/tree/main/examples?ref_type=heads)
1. Copy them in your working folder
1. Prepare the environment: [here](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md)
1. Use the `docker-start-command.sh` command sample to start the docker container
1. For the first part (Terraform), issue these commands: [here](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md#5-provision)
1. For the second part (Ansible), issue these commands (adapt based on your environment-name): [here](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_configure.md#4-configure)
