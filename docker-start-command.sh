docker run -it \
  -e AWS_ACCESS_KEY_ID="{YOURS}" \
  -e AWS_SECRET_ACCESS_KEY="{YOURS}" \
  -v {THIS_REPOSITORY_PWD}:/environments \
  registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest

#   -v /Users/massimosporchia/workdir/get-started/keys/.aws:/.aws \ <-- this is how I'd use it but it's not picking up the credentials correctly!